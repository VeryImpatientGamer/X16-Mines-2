# X16-Mines-2

A rewrite of X16-Mines in Assembly Language. 

X16-Mines was my first real X16 project so there are things I would do differently now. This version attempts to address that.

Right now the following additions are planned for this version (Subject to change):
- Various under the hood tweaks (like storing tile data in ram and vram instead of just vram).
- Full controller and keyboard support (Possibly mouse support in the menus as well).
- An improved tileset (and maybe the option to cycle through various tilesets).
- Question mark tiles. 
- Sound effects (maybe music as well).
- The ability to remap keys, controller buttons, or mouse buttons and to save these remappings for future use. 
- Improved menu setup with easier navigation. 
- A text manual that can be viewed from the X16 using it's built in text editor (instead of a PDF manual).
- High score system.
- Pause button. 

Until this version is complete I reccomend checking out Version 1 instead. 
https://codeberg.org/VeryImpatientGamer/X16-Mines